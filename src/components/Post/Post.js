import React from 'react';

const Post = props => {
    return (
        <div>
            <h1 style={{fontSize: '10px'}}>{props.author}</h1>
            <p>{props.message}</p>
        </div>
    );
};

export default Post;