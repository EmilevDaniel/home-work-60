import React from 'react';
import {useEffect} from "react";
import {useState} from "react";
import Post from "../Post/Post";

const axios = require('axios');

const Chat = () => {
    const url = 'http://146.185.154.90:8000/messages';
    const [posts, setPosts] = useState([]);
    const [message, setMessage] = useState({
            message: null,
            author: null,
        }
    );

    const changeState = (name, value) => {
        setMessage({...message, [name]: value})
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const posts = await response.json();
                posts.forEach(p => {
                    return p;
                });
                setPosts(posts);
            }
        };
        fetchData().catch(e => console.error(e));
    }, []);

    useEffect(() => {
        let lastTime = null;
        if (posts.length > 0) {
            lastTime = posts[posts.length - 1].datetime;
        }

        const interval = setInterval(() => {
            const fetchData = async () => {
                const response = await fetch(url + '?datetime=' + lastTime);

                if (response.ok && posts.length > 0) {
                    const post = await response.json();
                    console.log(post);
                    if (post.length > 0) {
                        post.forEach(e => {
                            setPosts([...posts, {author: e.author, message: e.message, datetime: e.datetime}]);
                        });
                    }
                }
            };
            fetchData().catch(e => console.error(e));
        }, 2000);
        return () => clearInterval(interval);
    }, [posts]);


    const sendMessage = async (message, author) => {
        try {
            const data = new URLSearchParams();
            data.set('author', author);
            data.set('message', message);
            const response = await axios.post(url, data);
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <div>
            <div>
                <label>Author:
                    <input onChange={e => changeState('author', e.target.value)}
                           type='text'/></label>
                <label>Message:
                    <input onChange={e => changeState('message', e.target.value)}
                           type='text'/>
                </label>
                <button onClick={() => sendMessage(message.message, message.author)}>Send</button>
            </div>

            {posts.map(post => (
                <Post
                    key={post._id}
                    message={post.message}
                    author={post.author}
                />
            ))}
        </div>
    );
};

export default Chat;